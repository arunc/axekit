# Approximate Computing Toolkit
------------------------------------------------------------------------------
## Arun Chandrasekharan < arun@uni-bremen.de >

### Group of Computer Architecture, (AGRA)
### Dept. of Computer Science - University of Bremen, Germany.
### http://www.informatik.uni-bremen.de/agra/eng/index.php
------------------------------------------------------------------------------
### License : Eclipse Public License (EPL 1.0)
------------------------------------------------------------------------------
### UNFINISHED PACKAGE 
------------------------------------------------------------------------------

A set of applications for synthesis/formal verification of approximate circuits.

doc/HowToBuild.txt has a quick build and run intro.

Read doc/axekit.pdf for detailed information on building and using axekit

Please cite the following if you are using axekit in publications and similar
areas.
DAC-2016, ASPDAC-2016, ICCAD-2016 (to complete)

```
Axekit uses several other opensource projects. Most important of these is Cirkit,
maintained by Mathias Soeken (http://msoeken.github.io/) and Berkeley ABC from Alan
Mischenko (https://people.eecs.berkeley.edu/~alanmi/abc/) .  Axekit has many
alogirthms/routines directly forked out of CirKit, ABC.

Other important external libs used are Yosys by Clifford Wolf
(http://www.clifford.at/yosys/), CUDD by Fabio Somenzi
(http://vlsi.colorado.edu/~fabio/), CryptoMiniSAT5 by Kuldeep Meeland others
(https://github.com/msoos/cryptominisat) and
sharpSAT by Marc Thurley (https://github.com/marcthurley/sharpSAT)

```
