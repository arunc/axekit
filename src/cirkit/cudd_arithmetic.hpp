// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cudd_arithmetic.cpp
// @brief  : Arithemtic functions using CUDD BDD (adder, subtractor) etc
//------------------------------------------------------------------------------

#pragma once
#ifndef CUDD_ARITHMETIC_HPP
#define CUDD_ARITHMETIC_HPP

#include <unordered_map>
#include <vector>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>

#include <cudd.h>
#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <cassert>
#include <stack>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>
#include <cirkit/bdd_utils.hpp>
#include <cirkit/bitset_utils.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <functions/characteristic.hpp>

#include <functions/bdd/bdd_logical.hpp>

#include <cuddInt.h>
#include <cuddObj.hh>


namespace axekit
{

using namespace cirkit;

std::pair <BDD, BDD> full_adder (const BDD &x, const BDD &y, const BDD&cin);

std::pair < std::vector<BDD>, BDD > full_adder ( const std::vector<BDD> &x,  const std::vector<BDD> &y,
						 const std::vector<BDD> &cin );

std::pair <bdd_function_t, bdd_function_t> full_adder (const bdd_function_t &x, const bdd_function_t &y,
						       const bdd_function_t &cin);

bdd_function_t negate_bdd (const bdd_function_t &bddf);

std::pair <bdd_function_t, bdd_function_t> subtract_bdd (const bdd_function_t &x,
							 const bdd_function_t &y);

bdd_function_t abs_bdd ( const std::pair <bdd_function_t, bdd_function_t> &x );

boost::multiprecision::uint256_t get_max_value ( const bdd_function_t &bddf );

boost::multiprecision::uint256_t get_max_value_chi ( const bdd_function_t &bddf );

boost::multiprecision::uint256_t get_weighted_sum ( const bdd_function_t &bddf );

boost::multiprecision::uint256_t get_weighted_sum2 ( const bdd_function_t &bddf );
boost::multiprecision::uint256_t get_weighted_sum3 ( const bdd_function_t &bddf );
boost::multiprecision::cpp_dec_float_100 get_weighted_sum4 ( const bdd_function_t &bddf );

bdd_function_t add_individual_outputs (const bdd_function_t &bddf);

}
#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
