// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cudd_cirkit.hpp
// @brief  : Cirkit routines ported to axekit
//           Original versions by Mathias
//           
//------------------------------------------------------------------------------


#pragma once
#ifndef CUDD_CIRKIT_HPP
#define CUDD_CIRKIT_HPP

#include <unordered_map>
#include <vector>

#include <boost/multiprecision/cpp_int.hpp>

#include <cudd.h>

#include <cirkit/properties.hpp>
#include <cirkit/bdd_utils.hpp>

namespace axekit
{
using namespace cirkit;

boost::multiprecision::uint256_t count_solutions ( const bdd_function_t& f);
boost::multiprecision::uint256_t count_solutions ( DdNode* f, const Cudd &mgr, unsigned nvars);
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
