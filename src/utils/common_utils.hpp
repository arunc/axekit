// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : common_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef COMMON_UTILS_HPP
#define COMMON_UTILS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iterator>

namespace axekit {
//------------------------------------------------------------------------------
// MP-UINT and MP-FLOAT datatypes.
// Note: normally do not need 100 precision for float. But supposed to be a
//       thin wrapper with negligible performance pernalty.
// Note: 512 is good enough for most of the circuits.
//       exception EPFL::memory_controller (#PO = 1231). TODO: deal with this.
//       512, 256, 1024 all have same performance penalty. Choose any of these.

using mpuint  = boost::multiprecision::uint256_t;
using mpfloat = boost::multiprecision::cpp_dec_float_100;

//------------------------------------------------------------------------------
  
mpuint bitset_to_mpuint ( const boost::dynamic_bitset<> &bs );
boost::dynamic_bitset<> mpuint_to_bitset ( const mpuint &num );
  
void create_work_dir (const std::string &dir_name);
void delete_work_dir (const std::string &dir_name);
char* curr_time ();
float get_elapsed_time ( const clock_t &begin_time );
void print_dbg (const std::string &str);
void cat_two_files ( const std::string &ofile, const std::string ifile1, 
		     const std::string ifile2 );
void cat_three_files ( const std::string &ofile, const std::string &ifile1, 
		       const std::string &ifile2, const std::string &ifile3 );

std::string get_extension_of_file (const std::string &file_name);
std::string get_name_of_file (const std::string &file_name);

inline unsigned count_lines ( const std::string &filename ) {
  std::ifstream file (filename);
  return std::count ( std::istreambuf_iterator<char> (file),
		      std::istreambuf_iterator<char>(), '\n'
    );
}

//inline mpfloat mpuint_to_mpfloat ( const mpuint &num ) { return static_cast<mpfloat> (num); }
inline mpfloat mpuint_to_mpfloat ( const mpuint &num  ) { return num.convert_to<mpfloat> (); }
inline mpuint  mpfloat_to_mpuint ( const mpfloat &num ) {
  // static cast, convert_to etc are errors.
  // anycase convert_to uses boost::lexical_cast only.
  //
  // idea: take the string, strip the decimal and put back the integer.
  auto str = boost::lexical_cast<std::string> (num);
  // if there is a 'e' and a '+' then this is well beyond the float representation.
  // convert from the string. e.g. 5.517519e+41
  if ( str.find('e') != std::string::npos ) {
    if ( str.find('-') != std::string::npos ) return 0; // fraction less than 1

    // now its a bit involved. mpuint does not accept 'e'
    // idea: form the equivalent string. 1.33e+5 = "133" + "000" and convert to mpuint. 
    auto float_str = str.substr (0, str.find("e", 0));
    const auto exp_str = str.substr ( str.find("+", 0), str.length() );
    const auto pos = float_str.find(".", 0);
    auto dec_digits = 0u;
    if (pos != std::string::npos) {
      dec_digits = float_str.substr ( pos, float_str.length() ).length();
      if (dec_digits > 0) dec_digits--;
    }
    boost::erase_all ( float_str, "." );
    auto exp = boost::lexical_cast<unsigned> (exp_str) - dec_digits;
    float_str = float_str + std::string (exp, '0');

    return mpuint(float_str);
  }
  
  auto mystr = str.substr (0, str.find(".", 0));
  mpuint res(mystr);
  return res;
}


}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
