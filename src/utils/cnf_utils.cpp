// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cnf_utils.cpp
// @brief  : Utilities for CNF for different solvers.
//------------------------------------------------------------------------------

#include "cnf_utils.hpp"

#include <proof/cec/cec.h>

namespace axekit
{

namespace {
inline int Cnf__Lit2Var (int Lit)  { return (Lit & 1)? -(Lit >> 1)-1 : (Lit >> 1)+1; }
inline int Cnf__Lit2Var2 (int Lit) { return (Lit & 1)? -(Lit >> 1)   : (Lit >> 1);   }
}

// Constraints the output bits as in output_pattern in CNF.
// Returns the number of inputs NOT used inside CNF.
// (2 ** return_val) has to be multiplied for model counting.
int write_cnf ( abc::Cnf_Dat_t *p, char *fileName, const int fReadable,
		const int num_inputs, const int num_outputs,
		const boost::dynamic_bitset<> &output_pattern ) {
  
  assert ( num_outputs == output_pattern.size() );
  FILE * pFile;
  int * pLit, * pStop, i, VarId;
  pFile = fopen( fileName, "w" );
  if ( pFile == nullptr ) {
    printf( "write_cnf(): Output file cannot be opened.\n" );
    return 0;
  }
  fprintf( pFile, "c CNF dump from aXc \n" );
  
  auto max_var = 0; // determine the max number of inputs really used.
  for ( i = 0; i < p->nClauses; i++ ) {
    for ( pLit = p->pClauses[i], pStop = p->pClauses[i+1]; pLit < pStop; pLit++ )
    {
      auto v = fReadable? Cnf__Lit2Var2(*pLit) : Cnf__Lit2Var(*pLit);
      if ( v > max_var ) max_var = v;
    }
  }

  fprintf( pFile, "c ind " );
  auto last_lit = *p->pClauses [ p->nClauses - 1 ];
  int konstant = fReadable ? Cnf__Lit2Var2(last_lit) : Cnf__Lit2Var(last_lit);
  if (konstant < 0) konstant = -konstant;
  for (auto i=0u; i<num_inputs; i++) {
    auto litId = konstant + i + 1;
    if (litId <= max_var) // do-not write out unused inputs.
      fprintf( pFile, "%d ",  litId);
  }
  assert (konstant <= max_var); // constant must be always included.
  fprintf( pFile, "%d ", konstant ); // konstant is also independent variable.
  fprintf( pFile, "0\n" );

  
  //fprintf( pFile, "p cnf %d %d\n", p->nVars, p->nClauses + num_outputs );
  fprintf( pFile, "p cnf %d %d\n", max_var, p->nClauses + num_outputs );
  for ( i = 0; i < p->nClauses; i++ ) {
    for ( pLit = p->pClauses[i], pStop = p->pClauses[i+1]; pLit < pStop; pLit++ )
    {
      fprintf( pFile, "%d ", fReadable? Cnf__Lit2Var2(*pLit) : Cnf__Lit2Var(*pLit) );
    }
    fprintf( pFile, "0\n" );
  }
  
  for (auto j=0; j<num_outputs; j++) {
    auto olit = j + 1;
    if ( !output_pattern.test(j) ) olit = -olit;
    fprintf( pFile, "%d 0\n", olit );
  }
  
  fclose( pFile );
  assert ( max_var <= (konstant + num_inputs) && "Unaccounted vars?" );
  return (konstant + num_inputs) - max_var;
}



//abc::Cnf_DataWriteIntoFile (cnf, cnffile, 0, NULL, NULL);
// Do not use for interfacing with other libs. Use only for ABC internal purpose.
void write_cnf ( abc::Cnf_Dat_t *p, char *fileName, int fReadable, int num_inputs ) {
  assert (false && "Only use for ABC internal purpose.");
    FILE * pFile;
    int * pLit, * pStop, i, VarId;
    pFile = fopen( fileName, "w" );
    if ( pFile == nullptr ) {
        printf( "write_cnf(): Output file cannot be opened.\n" );
        return;
    }
    fprintf( pFile, "c CNF dump from aXc \n" );
    fprintf( pFile, "c ind " );
    auto last_lit = *p->pClauses [ p->nClauses - 1 ];
    int konstant = fReadable ? Cnf__Lit2Var2(last_lit) : Cnf__Lit2Var(last_lit);
    if (konstant < 0) konstant = -konstant;
    for (auto i=0u; i<num_inputs; i++) {
      fprintf( pFile, "%d ", konstant + i + 1 );
    }
    fprintf( pFile, "0\n" );

    fprintf( pFile, "p cnf %d %d\n", p->nVars, p->nClauses );
    for ( i = 0; i < p->nClauses; i++ ) {
      for ( pLit = p->pClauses[i], pStop = p->pClauses[i+1]; pLit < pStop; pLit++ )
      {
	fprintf( pFile, "%d ", fReadable? Cnf__Lit2Var2(*pLit) : Cnf__Lit2Var(*pLit) );
      }
      fprintf( pFile, "0\n" );
    }

    fclose( pFile );
}


// remove the const-0 outputs from num_outputs.
// Returns the number of inputs NOT used inside CNF.
// (2 ** return_val) has to be multiplied for model counting.
int write_cnf ( abc::Cnf_Dat_t *p, char *fileName, const int fReadable,
		const int num_inputs, const int num_outputs ) {
  FILE * pFile;
  int * pLit, * pStop, i, VarId;
  pFile = fopen( fileName, "w" );
  if ( pFile == nullptr ) {
    printf( "write_cnf(): Output file cannot be opened.\n" );
    return 0;
  }
  fprintf( pFile, "c CNF dump from aXc \n" );
  
  auto max_var = 0; // determine the max number of inputs really used.
  for ( i = 0; i < p->nClauses; i++ ) {
    for ( pLit = p->pClauses[i], pStop = p->pClauses[i+1]; pLit < pStop; pLit++ )
    {
      auto v = fReadable? Cnf__Lit2Var2(*pLit) : Cnf__Lit2Var(*pLit);
      if ( v > max_var ) max_var = v;
    }
  }

  fprintf( pFile, "c ind " );
  auto last_lit = *p->pClauses [ p->nClauses - 1 ];
  int konstant = fReadable ? Cnf__Lit2Var2(last_lit) : Cnf__Lit2Var(last_lit);
  if (konstant < 0) konstant = -konstant;
  for (auto i=0u; i<num_inputs; i++) {
    auto litId = konstant + i + 1;
    if (litId <= max_var) // do-not write out unused inputs.
      fprintf( pFile, "%d ",  litId);
  }
  assert (konstant <= max_var); // constant must be always included.
  fprintf( pFile, "%d ", konstant ); // konstant is also independent variable.
  fprintf( pFile, "0\n" );

  
  //fprintf( pFile, "p cnf %d %d\n", p->nVars, p->nClauses + num_outputs );
  fprintf( pFile, "p cnf %d %d\n", max_var, p->nClauses + num_outputs );
  for ( i = 0; i < p->nClauses; i++ ) {
    for ( pLit = p->pClauses[i], pStop = p->pClauses[i+1]; pLit < pStop; pLit++ )
    {
      fprintf( pFile, "%d ", fReadable? Cnf__Lit2Var2(*pLit) : Cnf__Lit2Var(*pLit) );
    }
    fprintf( pFile, "0\n" );
  }
  
  for (auto j=0; j<num_outputs; j++) {
    fprintf( pFile, "%d 0\n", j + 1 );
  }
  
  fclose( pFile );
  assert ( max_var <= (konstant + num_inputs) && "Unaccounted vars?" );
  return (konstant + num_inputs) - max_var;
}



//------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
