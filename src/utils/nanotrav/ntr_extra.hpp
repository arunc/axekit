// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : ntr_extra.hpp
// @brief  : Some more extra utilites for reading and writing blif.
//
//------------------------------------------------------------------------------
#pragma once

#ifndef NTR_EXTRA_HPP
#define NTR_EXTRA_HPP

#include <iostream>
#include <cstdio>
#include <vector>
#include <utility>
#include <string>

#include <cuddInt.h>
#include <cirkit/bdd_utils.hpp>
#include "ntr.hpp"

namespace axekit {
using namespace nanotrav;

BnetNetwork * copy_bnet (BnetNetwork *ntk);
bool copy_DdInputs_to_bnet (DdManager *dd, BnetNetwork *ntk);
int Ntr_build_shared_DDs (
  BnetNetwork * net /**< network for which DDs are to be built */,
  DdManager * dd /**< %DD manager */,
  NtrOptions * option /**< option structure */);


}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
