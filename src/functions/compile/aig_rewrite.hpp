// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_rewrite.hpp
// @brief  : Approximation Rewriting in AIG (ICCAD-2016)
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef AIG_REWRITE_HPP
#define AIG_REWRITE_HPP

#include <cirkit/bdd_utils.hpp>
#include <iostream>
#include <tuple>
#include <fstream>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <algorithm>
#include <iomanip>

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include <utils/program_options.hpp>
#include <utils/common_utils.hpp>
#include <utils/yosys_utils.hpp>
#include <utils/miter_utils.hpp>
#include <utils/abc_utils.hpp>

#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>

#include <cirkit/cudd_cirkit.hpp>

#include <functions/has_limit_crossed.hpp>
#include <functions/path_routines.hpp>
#include <functions/cut_routines.hpp>
#include <functions/appx_miter.hpp>
#include <functions/cirkit_routines.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>


namespace axekit {

using namespace cirkit;

Network aig_rewrite ( Network ntk );

void to_aig_rewrite1 ( boost::multiprecision::cpp_dec_float_100 er_frac,
		       boost::multiprecision::int256_t wc,
		       boost::multiprecision::cpp_dec_float_100 ac,
		       boost::multiprecision::int256_t bf,
		       bool wc__flag, bool ac__flag, bool er__flag, bool wc__bf__flag );

void to_aig_rewrite2 ( int debug__in, int max__cut__size, int max__attempts, int effort__in );

std::tuple <float, float, float>  from_aig_rewrite ();

}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
