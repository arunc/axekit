// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_first_cut.cpp
// @brief  : 
//
// TODO : rewrite entire algorithm properly.
//
//        Reads in a Network (Abc_Aig type not Gia), writes out blif.
//        sorts the path, do a first cut, and then return.
//        Purpose is only to do some trivial approximation.
//------------------------------------------------------------------------------

#include "aig_first_cut.hpp"

namespace axekit
{

using namespace cirkit;

//------------------------------------------------------------------------------
using mpi = boost::multiprecision::int256_t;
using mpf = boost::multiprecision::cpp_dec_float_100;

namespace // Some of these should/may come from the compile command.
{ 

auto debug = 0;          // debug level for printing messages
auto max_cut_size = 3;   // constant for one run. To experiment with different cut-sizes.
std::string oblif;

float t_time_sort_paths = 0u, t_time_apply_cut = 0u, t_time_generate_cuts = 0u;

}

inline void ppp (int i) {
  std::cout  << "here:" << i << "\n";
}

Network aig_first_cut (Network ntk) {
  assert ( abc_ntk_inline::is_network_good(ntk) );
  // Note: anyway we lose the network name, port-name etc.
  // For now, no need to waste time on annotating these.
  abc_ntk_inline::set_network_name (ntk, "dummy");
  std::srand ( unsigned  (std::time(0)) );  
  const std::string work = ".aig_first_cut_work_" +  boost::lexical_cast<std::string> (rand());
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; 
  oblif = work_path + "appx_output_" + boost::lexical_cast<std::string> (rand()) + ".blif";
  auto design = abc_ntk_inline::make_design_from_network (ntk);
  
  std::string orig_vlog = work_path + "orig.v";
  if ( !abc_ntk_inline::check_network (ntk) )
    std::cout << "[e] Error. Original Network integrity failed..  :("
	      << std::endl;
  abc_ntk_inline::write_verilog_from_network (orig_vlog, ntk);
  auto orig_ntk_name = abc_ntk_inline::get_network_name ( ntk );
  auto orig_ntk_node_cnt = abc_ntk_inline::get_node_count ( ntk );
  
  //------------------------------------------------------------
  auto attempts = 0u;
  auto appx_ntk = abc_ntk_inline::copy_network ( ntk ); 
  abc_ntk_inline::set_network_name (
    appx_ntk, abc_ntk_inline::get_network_name (ntk) + "_appx" );

  appx_ntk = abc_ntk_inline::strash_network (appx_ntk); // Re-strash 
  assert ( abc_ntk_inline::is_topologically_sorted (appx_ntk) );
  // 3. Sort the longest paths.
  auto begin_time = std::clock ();
  auto po_list = abc_ntk_inline::get_po_list (appx_ntk);
  std::vector <Path> paths_list;
  for (auto &po : po_list) { 
    auto longest_path = get_longest_path_to_po (po);
    // prune const drivers to PO
    if ( abc_ntk_inline::is_node_const (
	   get_pi_of_path (longest_path) ) )  continue;
    paths_list.emplace_back (longest_path);
  }
  if ( 0u == po_list.size()) { // case where all PO drivers are consts
    std::cout << "[e] All primary-output drivers are constants!\n";
  } 
  auto nTop = 1u; // Just take top 1 path to work.
  sort_paths ( paths_list, nTop); 

  auto time_sort_paths = get_elapsed_time ( begin_time );
  t_time_sort_paths += time_sort_paths; // total time
  //------------------------------------------------------------
  // CUTS 
  // Get a flattend list of nodes to cut based on 1) longest path & 2) cut-volume.
  // 4. Sort based on cut-volume
  if (debug > 1) std::cout << "[i] Cut generation begin :: " << curr_time();
  begin_time = std::clock();
  auto cut_mgr = abc_ntk_inline::start_cut_manager (max_cut_size);
  auto total_nodes = abc_ntk_inline::get_node_count ( appx_ntk );
  std::vector <Object> node_list;
  for (auto i = 0u; i < nTop; i++) {
    auto cut_volume_list = get_cut_volume_list ( cut_mgr, paths_list[i] );
    reverse_sort_cut_volume_list (cut_volume_list);
    std::transform ( cut_volume_list.begin(), cut_volume_list.end(),
		     back_inserter(node_list),
		     [] (const CutVolume &cut) {return cut.first;} );
  }
  if (debug > 1) std::cout << "[i] Cut generation end :: " << curr_time();
  auto time_generate_cuts = get_elapsed_time ( begin_time );
  t_time_generate_cuts += time_generate_cuts; // total time
  
  // 6. Do the cutting.
  if (debug > 1) std::cout << "[i] Apply Cut begin :: " << curr_time();
  begin_time = std::clock();
  std::string cut_node_name;
  for (auto &node : node_list) { // loop only to avoid miscellaneous crasehes.
    if ( !abc_ntk_inline::is_node_good(node) ) continue;
    if ( !abc_ntk_inline::is_node_internal(node) ) continue;
    abc_ntk_inline::replace_node1_with_node2 ( appx_ntk, node,
					       abc_ntk_inline::get_const1 (appx_ntk) );
    break; // after first cut, exit
  } // for (auto &node : node_list)
  
  auto time_apply_cut = get_elapsed_time ( begin_time );
  t_time_apply_cut += time_apply_cut; // total time
   
  if ( !abc_ntk_inline::check_network (appx_ntk) )
    std::cout << "[e] Error. Approximated Network integrity failed..  :(" << std::endl;
  return appx_ntk;
  
}

//------------------------------------------------------------------------------
  
// return the output appx-network name, and all the timings.
std::tuple <float, float, float>  from_aig_first_cut () {
  return std::make_tuple (
    t_time_sort_paths, t_time_apply_cut, t_time_generate_cuts
    );
}
  
//------------------------------------------------------------------------------
} // namespace axekit



// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
