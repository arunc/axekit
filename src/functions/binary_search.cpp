// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : binary_search.cpp
// @brief  : All binary search routines with SAT
//------------------------------------------------------------------------------

#include "binary_search.hpp"

namespace axekit
{
  
//------------------------------------------------------------------------------
unsigned binsearch_max_bit_flip ( const std::string &golden_verilog, const Design &approx_design, 
				  Network approx_ntk, const Port &port_to_check, 
				  const std::string &work, const unsigned &debug) {
  
  auto max_iterations = 500u; // enough for 1000 bit widths.
  
  auto lower_bound = 0u;
  auto curr_error = get_port_width (port_to_check); // max-case, all bits flipped.
  auto upper_bound = curr_error;

  init_appx_miter (work, debug, golden_verilog);
  // Binary Search.
  auto step = 1u;
  while (lower_bound < upper_bound)
  {
    auto sum = upper_bound + lower_bound;
    auto lsb = (sum & 1u);
    curr_error = lsb ? ( (sum >> 1) + 1 ) : (sum >> 1) ; // (ceil (u+l)/2)
    //std::cout << "sum = " << sum << "  ::  lsb = " << lsb << std::endl;
    //curr_error = (sum >> 1u) | lsb; // (ceil (u+l)/2)
    //std::cout << "step : " << step << "  curr_error : " << curr_error 
    //      << " upper_bound : " << upper_bound << "  lower_bound : " << lower_bound << "\n";

    auto result = solve_ge_bf_appx_miter (approx_design, port_to_check, curr_error, approx_ntk);

    // we are always verifying > 
    // 

    // 3. Adjust the bounds
    if (result == SolverResult::NO) { // lower_bound remains the same.
      upper_bound = curr_error - 1; // since we are verifying >= 
    }
    else if (result == SolverResult::YES) { // upper bound remains the same.
      lower_bound = curr_error;
    }
    else if (result == SolverResult::TIMEOUT) { // Cant figure out by this method.
      std::cout << "[e] TO. Inference :: worst-case is between " << lower_bound
		<< " and " << upper_bound << std::endl;
      std::cout << "[e] Cannot process further. Returning 0 " << std::endl;
      return 0u;
    }
    else if (result == SolverResult::ERROR) {
      std::cout << "[e] Solver Error!. Returing 0\n";
      return 0u;
    }

    //auto lsb = curr_error & 1u;
    //curr_error = lsb ? ( (curr_error >> 1u) + 1 ) : (curr_error >> 1u) ;
    //---auto sum = curr_error;
    //---auto lsb = (sum & 1u);
    //---curr_error = lsb ? ( (sum >> 1) + 1 ) : (sum >> 1) ; // (ceil (u+l)/2)
    //---std::cout << "sum = " << sum << "  ::  lsb = " << lsb << std::endl;
    //---//curr_error = (sum >> 1u) | lsb; // (ceil (u+l)/2)
    
    if (step >= max_iterations) {
      std::cout << "[e] Inference :: worst-case is between " << lower_bound
		<< " and " << upper_bound << std::endl;
      std::cout << "[e] Aborting after " << step << "th step. Returning 0 "
		<< std::endl;
      return 0u;
    }
    step++;
    
  } // while loop
  
  assert (lower_bound == upper_bound); // Never fails.
  return lower_bound;

}

//------------------------------------------------------------------------------
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
