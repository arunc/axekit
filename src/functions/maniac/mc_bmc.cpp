
#include "mc_bmc.hpp"

namespace maniac
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/



namespace {
// redirect printf() output to a string.
// http://stackoverflow.com/questions/19485536/
//        redirect-output-of-an-function-printing-to-console-to-string

const auto BUFFSIZE = 4096 * 2;
FILE *org_stdout, *org_stderr;
FILE *tmp_stdout, *tmp_stderr;
char buffer_stdout[BUFFSIZE];
char buffer_stderr[BUFFSIZE];

inline void close_stdout() {
  org_stdout = stdout;
  tmp_stdout = fmemopen (buffer_stdout, BUFFSIZE, "w");
  stdout = tmp_stdout;
}
inline void close_stderr() {
  org_stderr = stderr; 
  tmp_stderr = fmemopen (buffer_stderr, BUFFSIZE, "w");
  stderr = tmp_stderr;
}

inline void restore_stdout () {
  std::fclose (tmp_stdout);
  stdout = org_stdout;
}

inline void restore_stderr () {
  std::fclose (tmp_stderr);
  stderr = org_stderr;
}

}


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
//---inline void ___run_abc_cmd (abc::Abc_Frame_t *frame,
//---			   std::string cmd, bool debug)
//---{
//---  
//---  if (debug) std::cout << "[i] Abc :: " << cmd << std::endl;
//---  auto status = abc::Cmd_CommandExecute ( frame, cmd.c_str() );
//---  // Interpretation of status is wrong. Its not a boolean for true or false.
//---  //if (debug && !status)  std::cout << "[e] Abc :: command " << cmd
//---  //				   << " failed " << std::endl;
//---  
//---}
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
int mc_bmc( const std::string &blif_file, const std::string &out_cex_file,
	    const std::string &results_file, unsigned cycle_limit, bool debug,
	    bool print_cex) {
  if (0 == debug) {close_stderr(); close_stdout();}
  auto ntk = abc::Io_ReadBlif ( (char *)blif_file.c_str(), 1 );
  if (0 == debug) {restore_stderr(); restore_stdout();}

  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  if (ntk == nullptr) std::cout << "Null pointer!\n";
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  assert ( abc::Abc_NtkLatchNum (ntk) != 0 ); // run only for sequential designs. 
  auto result = axekit::bmc ( ntk, 10000, false );
  if (result.first) return result.second;
  else return -1;
}


//---int mc_bmc( const std::string &blif_file, const std::string &out_cex_file,
//---	    const std::string &results_file, unsigned cycle_limit, bool debug,
//---	    bool print_cex)
//---{
//---  abc::Abc_Frame_t *frame = abc::Abc_FrameGetGlobalFrame();
//---  if (!frame) {
//---    abc::Abc_Stop();
//---    std::cout << "[e] not able to invoke the ABC frame" << std::endl;
//---    assert (false ); 
//---  }
//---
//---  // redirect printf() output to a string.
//---  // http://stackoverflow.com/questions/19485536/
//---  //        redirect-output-of-an-function-printing-to-console-to-string
//---  auto org_stdout = stdout; 
//---  char buffer_results[4096];
//---  auto tmp_stdout = fmemopen (buffer_results, 4096, "w");
//---  stdout = tmp_stdout;
//---
//---  std::string read_blif = "read_blif " + blif_file;
//---  std::string strash = "strash ";
//---  std::string fraig  = "ifraig ";
//---  std::string bmc = "bmc3 -F " + std::to_string (cycle_limit);
//---  std::string cex = "write_cex -n  " + out_cex_file;
//---
//---  auto abc_debug = false;
//---  ___run_abc_cmd (frame, read_blif, abc_debug);
//---  ___run_abc_cmd (frame, strash,    abc_debug);
//---  ___run_abc_cmd (frame, fraig,     abc_debug);
//---  ___run_abc_cmd (frame, bmc,       abc_debug);
//---  //if (debug) ___run_abc_cmd (frame, cex, abc_debug);
//---  
//---  std::fclose (tmp_stdout);
//---  stdout = org_stdout;
//---  std::string results = buffer_results;
//---  std::ofstream of;
//---  of.open (results_file.c_str(), std::ios_base::out);
//---  of << results;
//---  of.close();
//---
//---  std::string no_output =  "No output asserted";
//---  std::string proved = "Explored all reachable states";
//---  std::string satisfied = "was asserted in frame";
//---
//---  if (debug)
//---  {
//---    std::cout << "[i] Abc Info begins :: " << std::endl
//---	      << results << "[i] Abc Info ends " << std::endl;
//---  }
//---  if ( results.find (no_output) != std::string::npos ) {
//---    if (debug) std::cout << "[i] BMC Result -  No output asserted after "
//---			 << cycle_limit << " Frames" << std::endl;
//---    return -2;
//---  }
//---  if ( results.find (proved) != std::string::npos  ) {
//---    //if (debug) std::cout << "[i] PDR Result -  NO" << std::endl;
//---    return -1;
//---  }
//---  if ( results.find (satisfied) != std::string::npos  ) {
//---    auto str = results;
//---    auto my_regex = boost::regex (
//---      "^.*Output 0.*was asserted.*frame");
//---    auto tmp1 = boost::regex_replace ( str, my_regex, std::string("") );
//---    auto cycles = std::stoi ( boost::regex_replace (
//---				tmp1, boost::regex("\\.*Time.*=.*"),
//---				std::string("")
//---				) );
//---    if (print_cex)   ___run_abc_cmd (frame, cex,       abc_debug);
//---    return cycles;
//---  }
//---
//---  return -3;
//---}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
