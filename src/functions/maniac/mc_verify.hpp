// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 

/**
 * @file mc_verify.hpp
 *
 * @brief Formal verifier for Sequential Approximate ckts.
 *
 * @author Arun
 */

#ifndef MC_VERIFY_HPP
#define MC_VERIFY_HPP

#include <string>
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>
#include <cstdlib>
#include <utility>
#include <fstream>

#include <utils/common_utils.hpp>

#include <ext-libs/yosys/yosys_api.hpp>
#include <functions/maniac/mc_design.hpp>
#include <functions/maniac/mc_make_miter.hpp>
#include <functions/maniac/mc_accum_bitflip_cycles.hpp>
#include <functions/maniac/mc_accum_error_cycles.hpp>
#include <functions/maniac/mc_max_bitflip.hpp>

namespace maniac
{

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
class ManiacVerify
{
public:
  void verify();
  void init_work_dirs();
  std::pair <unsigned long, float> get_worst_case  () { return wc_res; }
  std::pair <unsigned long, float> get_max_bitflip () { return bf_res; }
  std::pair <int, float> get_acc_error   () { return accum_error_res; }
  std::pair <int, float> get_acc_bitflip () { return accum_bf_res; }
  
  
  // command arguments.
  std::string golden;
  std::string approx;
  std::string golden_module;
  std::string approx_module;
  std::string port;
  std::string clock = "clock";
  std::string reset = "rst";
  std::string oe = "__NA__";
  unsigned error_cyclelimit = 100u;
  unsigned bitflip_cyclelimit = 100u;
  unsigned accum_bitflip = 0u;
  unsigned accum_error = 0u;
  unsigned avg_error = 0u;
  std::string log_file = "cirkit_maniac.log";
  std::string report_file = "maniac.rpt";
  std::string csv_file = "maniac.csv";
  unsigned debug = 1u; // Default in debug mode.
  unsigned signed_outputs = 0u; // To indidcate if inputs are signed or not.

  unsigned worst_case_en = 1u; // If worst-case computations are enabled.
  unsigned accum_error_en = 1u; // If acc error computations are enabled.
  unsigned max_bf_en = 1u; // If worst-bitflip computations are enabled.
  unsigned accum_bitflip_en = 1u; // If accum-bitflip computations are enabled.
  unsigned avg_case_en = 1u; // If avg-case computations are enabled.
  bool cross_verify = false;  //  cross-verify the accum computations results.

  // avg_error = accumulated_error / cycles_for_avg_error;
  // if its 0u, then division will be rounded to nxt shifting.
  // if its 1u, then division will be with a constant
  // if its 2u, then generic division will 
  unsigned avg_case_effort_level = 0u;
  unsigned cycles_for_avg_error = 256u;

  unsigned opt = 0u;
  unsigned bf_upper_bound = 0u;
  unsigned bf_lower_bound = 0u;
  
private:
  // Private Variables.
  //mc_design gold, appx;
  std::string work = "work";
  std::string worst_case_work = "work/worst_case";
  std::string accum_case_work = "work/accum_case";
  std::string max_bf_work = "work/max_bf";
  std::string accum_er_work = "work/accum_er";
  std::string avg_case_work = "work/avg_case";

  float wc_time = 0, acc_time = 0, bf_time = 0, acc_bf_time = 0;
  
  // Not for CLI. Private variables.
  mc_port port_to_check = std::make_pair("__NA__", 0);

  void print_program_options (std::ofstream &file);
  inline bool true_or_false (unsigned value);
  bool check_sanity (const mc_design &golden, const mc_design &approx);
  inline void update_avg_case_clock_cycles ();

  bool dump_report = false;

  std::pair <unsigned long, float> wc_res = {0, 0.0};
  std::pair <int, float> accum_error_res = {0, 0.0};
  std::pair <unsigned long, float> bf_res = {0, 0.0};
  std::pair <int, float> accum_bf_res = {0, 0.0};

  
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


