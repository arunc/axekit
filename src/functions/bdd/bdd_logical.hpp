// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : bdd_logical.hpp
// @brief  : Logical functions to operate on bdd_function_t datatype.
//          
//------------------------------------------------------------------------------
#pragma once
#ifndef BDD_LOGICAL_HPP
#define BDD_LOGICAL_HPP

#include <vector>
#include <cirkit/bdd_utils.hpp>
#include <functions/cirkit_routines.hpp>

namespace axekit {

using namespace cirkit;

bdd_function_t xor_bdd (const bdd_function_t &x, const bdd_function_t &y);
bdd_function_t and_bdd (const bdd_function_t &x, const bdd_function_t &y);
bdd_function_t or_bdd  (const bdd_function_t &x, const bdd_function_t &y);
bdd_function_t not_bdd (const bdd_function_t &bddf);
  
}

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
