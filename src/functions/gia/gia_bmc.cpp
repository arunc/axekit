// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_bmc.cpp
// @brief  : Bounded Model Checking
//
//------------------------------------------------------------------------------

#include "gia_bmc.hpp"

#include <cstdio>
#include <sat/bmc/bmc.h>
#include <proof/pdr/pdr.h>

namespace axekit
{

namespace {
// redirect printf() output to a string.
// http://stackoverflow.com/questions/19485536/
//        redirect-output-of-an-function-printing-to-console-to-string

const auto BUFFSIZE = 4096 * 2;
FILE *org_stdout, *org_stderr;
FILE *tmp_stdout, *tmp_stderr;
char buffer_stdout[BUFFSIZE];
char buffer_stderr[BUFFSIZE];

inline void close_stdout() {
  org_stdout = stdout;
  tmp_stdout = fmemopen (buffer_stdout, BUFFSIZE, "w");
  stdout = tmp_stdout;
}
inline void close_stderr() {
  org_stderr = stderr; 
  tmp_stderr = fmemopen (buffer_stderr, BUFFSIZE, "w");
  stderr = tmp_stderr;
}

inline void restore_stdout () {
  std::fclose (tmp_stdout);
  stdout = org_stdout;
}

inline void restore_stderr () {
  std::fclose (tmp_stderr);
  stderr = org_stderr;
}

}

// BMC3 routine : returns <SAT/UNSAT, num_frames to SAT>
// BMC returns true if atleast one of the output is asserted.
// Does not track in case of multiple outputs, if some are SAT or UNSAT.
// This version checks strictly for max_frames.
// Cannot distinguish from other cases.
// Cannot use the cex generated also further.
std::pair <bool, int> bmc ( abc::Abc_Ntk_t *pNtk, const int max_frames, const bool verify_cex ) {
  assert ( abc::Abc_NtkLatchNum (pNtk) != 0 ); // Must be sequential.
  abc::Aig_Man_t *pMan;
  pMan = abc::Abc_NtkToDar (pNtk, 0, 1);
  if (pMan == nullptr) {
    std::cout << "[e] BMC error. Conversion failed.\n";
    return {false, -1};
  }
  assert ( pMan->nRegs > 0 );

  close_stdout(); close_stderr();
  // first run a PDR and verify if this holds or not.
  // Since PDR is much faster.
  abc::Pdr_Par_t PdrPars; auto *pPdrPars = &PdrPars;
  abc::Pdr_ManSetDefaultParams (pPdrPars);
  auto pdr_ret = abc::Pdr_ManSolve ( pMan, pPdrPars );
  if (pdr_ret != 0) return {false, -1};
  
  
  // make pPars
  abc::Saig_ParBmc_t Pars;  auto *pPars = &Pars;
  abc::Saig_ParBmcSetDefaultParams ( pPars );
  pPars->nFramesMax = max_frames;

  auto ret = abc::Saig_ManBmcScalable ( pMan, pPars );
  
  restore_stdout(); restore_stderr();

  ABC_FREE ( pNtk->pModel );
  ABC_FREE ( pNtk->pSeqModel );
  pNtk->pSeqModel = pMan->pSeqModel; // move results here.
  pMan->pSeqModel = nullptr;

  bool status = false;
  int num_frames = -1;
  if ( 1 == ret ) { // Exhaustively verified all the states.
    num_frames = ( 1 << abc::Aig_ManRegNum(pMan) );
    status = false;
  } 
  else if ( -1 == ret ) { // Resource limit reached.
    if ( 0 == pPars->nFailOuts ) {
      status = false;
      num_frames = (pPars->iFrame + 1) > 0 ? (pPars->iFrame + 1) : 0;
    }
    else { // Atleast ONE output is asserted.
      status = true;
      num_frames = pPars->iFrame;
    }
  }
  else if ( 0 == ret ) { // Solved completely and atleast one output is asserted.
    status = true;
    auto pCex = pNtk->pSeqModel;
    num_frames = pCex->iFrame;
  }
  else {
    assert (false && "Unknown behavior from BMC3");
  }
  
  if ( pNtk->pSeqModel && verify_cex )  {
    status = abc::Saig_ManVerifyCex( pMan, pNtk->pSeqModel );
    if ( status == 0 ) std::cout << "[e] CEX Verification failed\n";
  }

  abc::Aig_ManStop( pMan );
  return {status, num_frames};
  
}
  


std::pair <bool, int> bmc ( abc::Gia_Man_t *gia, const int max_frames, const bool verify_cex ) {
  assert ( gia->nRegs > 0 ); // must be sequential.
  // Until we find a proper way to find gia to ntk conversion, take this route.
  // See aig_to_ntk.cpp for other attempts.
  char ntk_file[] = "/tmp/bmc3.aig";
  abc::Gia_AigerWrite ( gia, ntk_file, 1, 0 );    
  auto ntk = abc::Io_ReadAiger ( ntk_file, 1 );
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  ntk->pName = abc::Abc_UtilStrsav ( gia->pName );
  assert ( abc::Abc_NtkLatchNum (ntk) != 0 );
  
  auto status = bmc (ntk, max_frames, verify_cex);
  abc::Abc_NtkDelete (ntk); // cleanup
  return status;
}

}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
