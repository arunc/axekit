// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_logical.cpp
// @brief  : Logical functions to operate on multioutput Gia.
//
// TODO: define operators for all these
//------------------------------------------------------------------------------

#include "gia_logical.hpp"

namespace axekit
{

// PIs will be retained as the same, and POs will be XORed.
// Get the seqeuential version of this procedure from giaDup.c
// XOR : op = 0
// AND : op = 1
// OR  : op = 2
abc::Gia_Man_t * do_logic_operation (abc::Gia_Man_t *pOne, abc::Gia_Man_t *pTwo, unsigned op) {
  assert ( abc::Gia_ManPiNum (pOne) == abc::Gia_ManPiNum (pTwo) && "Number of inputs different");
  assert ( abc::Gia_ManPoNum (pOne) == abc::Gia_ManPoNum (pTwo) && "Number of outputs different");

  assert (op < 3);
  assert (abc::Gia_ManRegNum (pOne) == 0  && "Sequential not supported");
  assert (abc::Gia_ManRegNum (pTwo) == 0  && "Sequential not supported");
  
  abc::Gia_Obj_t * pObj;
  int i;
  auto pNew = abc::Gia_ManStart ( abc::Gia_ManObjNum( pOne ) + abc::Gia_ManObjNum( pTwo ) );
  pNew->pName = abc::Abc_UtilStrsav ( pOne->pName );
  pNew->pSpec = abc::Abc_UtilStrsav ( pOne->pSpec );
  abc::Gia_ManHashAlloc( pNew );

  // Copy all the objects from both Gias.
  abc::Gia_ManConst0( pOne )->Value = 0;
  Gia_ManForEachObj1( pOne, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )  {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pOne, pObj ) )  {
      pObj->Value = abc::Gia_ManAppendCi( pNew );
    }
  }
  abc::Gia_ManConst0( pTwo )->Value = 0;
  Gia_ManForEachObj1( pTwo, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )  {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pTwo, pObj ) ) {
      pObj->Value = abc::Gia_ManPi( pOne, abc::Gia_ObjCioId( pObj ) )->Value;
    }
  }

  Gia_ManForEachCo( pOne, pObj, i )
  {
    auto x1 = abc::Gia_ObjFanin0Copy ( abc::Gia_ManCo (pTwo, i) );
    auto x2 = abc::Gia_ObjFanin0Copy (pObj);
    int res = 0;
    if ( 0 == op ) res = abc::Gia_ManHashXor ( pNew, x1, x2 );
    if ( 1 == op ) res = abc::Gia_ManHashAnd ( pNew, x1, x2 );
    if ( 2 == op ) res = abc::Gia_ManHashOr  ( pNew, x1, x2 );
    Gia_ManAppendCo (pNew, res);
  }

  return pNew;
  
}


// PIs will be retained as the same, and POs will be XORed.
// XOR is actually the miter.
abc::Gia_Man_t * xor_gia (abc::Gia_Man_t *x, abc::Gia_Man_t *y)
{
  auto res = do_logic_operation ( x, y, 0 );
  char name[] = "xor_gia";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}

// PIs will be retained as the same, and POs will be ANDed.
abc::Gia_Man_t * and_gia (abc::Gia_Man_t *x, abc::Gia_Man_t *y)
{
  auto res = do_logic_operation ( x, y, 1 );
  char name[] = "and_gia";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}

// PIs will be retained as the same, and POs will be ORed.
abc::Gia_Man_t * or_gia (abc::Gia_Man_t *x, abc::Gia_Man_t *y)
{
  auto res = do_logic_operation ( x, y, 2 );
  char name[] = "or_gia";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}

// PIs will be retained as the same, and POs will be inverted
abc::Gia_Man_t * not_gia (abc::Gia_Man_t *pOne)
{
  assert (abc::Gia_ManRegNum (pOne) == 0  && "Sequential not supported");
  abc::Gia_Obj_t * pObj;
  int i;
  auto pNew = abc::Gia_ManStart ( abc::Gia_ManObjNum( pOne ) );
  char name[] = "not_gia";
  pNew->pName = abc::Abc_UtilStrsav ( name );
  pNew->pSpec = abc::Abc_UtilStrsav ( pOne->pSpec );
  abc::Gia_ManHashAlloc( pNew );

  // Copy all the objects from pOne.
  abc::Gia_ManConst0( pOne )->Value = 0;
  Gia_ManForEachObj1( pOne, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )  {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pOne, pObj ) )  {
      pObj->Value = abc::Gia_ManAppendCi( pNew );
    }
  }

  Gia_ManForEachCo( pOne, pObj, i )
  {
    Gia_ManAppendCo (pNew, abc::Gia_ObjFanin0Copy ( abc::Gia_Not (pObj) ) );
  }

  return pNew;
  
}



// PIs will be retained as the same, and POs will be individually XORed/ANDed/ORed
// Get the seqeuential version of this procedure from giaDup.c
// XOR : op = 0
// AND : op = 1
// OR  : op = 2
abc::Gia_Man_t * do_indiv_outputs_logic (abc::Gia_Man_t *pOne, unsigned op) {

  assert (op < 3);
  assert (abc::Gia_ManRegNum (pOne) == 0  && "Sequential not supported");

  abc::Gia_Obj_t * pObj;
  int i;
  auto pNew = abc::Gia_ManStart ( abc::Gia_ManObjNum( pOne ) );
  char name[] = "sum_of_outputs";
  pNew->pName = abc::Abc_UtilStrsav ( name );
  pNew->pSpec = abc::Abc_UtilStrsav ( pOne->pSpec );
  abc::Gia_ManHashAlloc( pNew );

  // Copy all the objects from pOne
  abc::Gia_ManConst0( pOne )->Value = 0;
  Gia_ManForEachObj1( pOne, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )  {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pOne, pObj ) )  {
      pObj->Value = abc::Gia_ManAppendCi( pNew );
    }
  }

  if ( 1 == abc::Gia_ManPoNum(pOne) ) { // case of only one output.
    abc::Gia_ManAppendCo (pNew, abc::Gia_ObjFanin0Copy ( abc::Gia_ManCo (pOne, 0) ) );
    return pNew;
  }

  int lit = 0; // constant 0 literal. for XOR and OR.
  if (1 == op) lit = abc::Abc_LitNot (lit);  // only for AND 
  
  
  Gia_ManForEachCo( pOne, pObj, i )
  {
    auto x1 = abc::Gia_ObjFanin0Copy (pObj);
    if ( 0 == op ) lit = abc::Gia_ManHashXor ( pNew, lit, x1 );
    if ( 1 == op ) lit = abc::Gia_ManHashAnd ( pNew, lit, x1 );
    if ( 2 == op ) lit = abc::Gia_ManHashOr  ( pNew, lit, x1 );
  }

  abc::Gia_ManAppendCo ( pNew, lit );
  return pNew;
}



// PIs will be retained as the same, and POs will be XORed.
// input = pOne_output[n-0]
// output = pOne_output[0] ^ pOne_output[1] ^ pOne_output[2] ^ .. ^ pOne_output[n]
abc::Gia_Man_t * xor_individual_outputs (abc::Gia_Man_t *x)
{
  auto res = do_indiv_outputs_logic ( x, 0 );
  char name[] = "xor_of_outputs";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}

// PIs will be retained as the same, and POs will be ANDed together
// input = pOne_output[n-0]
// output = pOne_output[0] & pOne_output[1] & pOne_output[2] & .. & pOne_output[n]
abc::Gia_Man_t * and_individual_outputs (abc::Gia_Man_t *x)
{
  auto res = do_indiv_outputs_logic ( x, 1 );
  char name[] = "and_of_outputs";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}

// PIs will be retained as the same, and POs will be ORed together
// input = pOne_output[n-0]
// output = pOne_output[0] | pOne_output[1] | pOne_output[2] | .. | pOne_output[n]
abc::Gia_Man_t * or_individual_outputs (abc::Gia_Man_t *x)
{
  auto res = do_indiv_outputs_logic ( x, 2 );
  char name[] = "or_of_outputs";
  res->pName = abc::Abc_UtilStrsav ( name );
  return res;
}


}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
