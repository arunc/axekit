// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_countsat.hpp
// @brief  : CounSat procedures with Gia
//------------------------------------------------------------------------------

#pragma once
#ifndef GIA_COUNTSAT_HPP
#define GIA_COUNTSAT_HPP

#include <unordered_map>
#include <vector>
#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>

#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <cassert>
#include <stack>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>
#include <cirkit/bitset_utils.hpp>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <sat/cnf/cnf.h>
#include <sat/bsat/satSolver.h>
#include <ext-libs/abc/abc_api.hpp>

#define LN( x ) if ( verbose ) { std::cout << x << std::endl; }

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

namespace axekit
{


boost::dynamic_bitset<> experimental_solve_maxlexsat_int ( abc::Gia_Man_t *gia );
boost::multiprecision::uint256_t sum_of_max_values ( abc::Gia_Man_t *gia,
						     const boost::multiprecision::uint256_t &max_terms,
						     const unsigned tool_type = 1u );


// All outputs are constrained to the respective value in output_pattern.
boost::multiprecision::uint256_t countsat_with_sharpsat ( abc::Gia_Man_t *gia,
							  const boost::dynamic_bitset <> &output_pattern );
// All outputs are constrained to be true in the CNF.
// If some outputs are 0 constants or to be constrained as 0, use above routine.
boost::multiprecision::uint256_t countsat_with_sharpsat ( abc::Gia_Man_t *gia );


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
