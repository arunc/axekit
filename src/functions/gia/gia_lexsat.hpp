// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_lexsat.cpp
// @brief  : LexSat procedures with Gia
//------------------------------------------------------------------------------

#pragma once
#ifndef GIA_LEXSAT_HPP
#define GIA_LEXSAT_HPP

#include <unordered_map>
#include <vector>
#include <algorithm>
#include <boost/dynamic_bitset.hpp>
#include <boost/lexical_cast.hpp>

#include <functional>
#include <iostream>
#include <sstream>
#include <utility>
#include <cassert>
#include <stack>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>
#include <cirkit/bitset_utils.hpp>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <sat/cnf/cnf.h>
#include <sat/bsat/satSolver.h>
#include <ext-libs/abc/abc_api.hpp>

#define LN( x ) if ( verbose ) { std::cout << x << std::endl; }


namespace axekit
{
boost::dynamic_bitset<> solve_max_lexsat ( abc::Gia_Man_t *gia );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
