// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : simulate.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "abc_api.hpp"

// If we are using Ntk, then mostly we need to start ABC.
// When we use Gia, somewhere in between this is taken care of (??)
// Also, many cases its not even necessary to start ABC explicitly.
namespace axekit {

void write_verilog ( abc::Gia_Man_t *gia, char *file ) {
  auto kk = abc::Gia_ManToAig (gia, 0);
  abc::Aig_ManDumpVerilog (kk, file);
}
void write_verilog ( abc::Gia_Man_t *gia, const std::string &file ) {
  write_verilog (gia, (char*) file.c_str() );
}
void write_verilog ( abc::Abc_Ntk_t *origNtk, char *file ) {

  assert ( abc::Abc_NtkIsLogic(origNtk) );
  auto ntk = abc::Abc_NtkDup ( origNtk ); 
  if (ntk == nullptr) return ;
  abc::Abc_NtkStrash (ntk, 1, 1, 0); 
  abc::Abc_NtkCleanup (ntk, 0);      
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); 
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); 
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); 
  auto dar = abc::Abc_NtkToDar ( ntk, 0, 0 ); // 8. network_to_gia()
  abc::Abc_NtkDelete (ntk);
  auto gia = abc::Gia_ManFromAig ( dar );

  write_verilog ( gia, file);

}

void write_verilog ( abc::Abc_Ntk_t *ntk, const std::string &file ) {
  write_verilog (ntk, (char*) file.c_str() );
}


bool start_abc() {
  abc::Abc_Start();
  auto frame = abc::Abc_FrameGetGlobalFrame();
  if (frame == nullptr) {
    abc::Abc_Stop();
    return false;
  }
  return true;
}

void stop_abc() {
  abc::Abc_Stop();
}

bool is_abc_started() {
  if (abc::Abc_FrameGetGlobalFrame() == nullptr) return false;
  return true;
}

}



// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
