/**
 * @file yosys_api.hpp
 *
 * @brief Yosys interface
 *
 * Interface to Yosys
 * Ported from Yosys project.
 *
 * @author Arun 
 * @since  2.3
 */

#ifndef YOSYS_API_HPP
#define YOSYS_API_HPP

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//#include <boost/regex.hpp>
#include <boost/algorithm/string/erase.hpp>

#include <map>
#include <set>
#include <vector>
#include <string>
#include <algorithm>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <initializer_list>
#include <stdexcept>
#include <memory>

#include <sstream>
#include <fstream>
#include <istream>
#include <ostream>
#include <iostream>

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>


#define PRIVATE_NAMESPACE_BEGIN  namespace {
#define PRIVATE_NAMESPACE_END    }
#define YOSYS_NAMESPACE_BEGIN    namespace Yosys {
#define YOSYS_NAMESPACE_END      }
#define YOSYS_NAMESPACE_PREFIX   Yosys::
#define USING_YOSYS_NAMESPACE    using namespace Yosys;

#define YS_OVERRIDE override
#define YS_FINAL final

#if defined(__GNUC__) || defined(__clang__)
#  define YS_ATTRIBUTE(...) __attribute__((__VA_ARGS__))
#  define YS_NORETURN
#elif defined(_MSC_VER)
#  define YS_ATTRIBUTE(...)
#  define YS_NORETURN __declspec(noreturn)
#else
#  define YS_ATTRIBUTE(...)
#  define YS_NORETURN
#endif

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
namespace Yosys {

// Note: All headers included in hashlib.h must be included
// outside of YOSYS_NAMESPACE before this or bad things will happen.
#ifdef HASHLIB_H
#  undef HASHLIB_H
#  include <ext-libs/yosys/utils/hashlib.hpp>
#else
#  include <ext-libs/yosys/utils/hashlib.hpp>
#  undef HASHLIB_H
#endif

using std::vector;
using std::string;
using std::pair;

  
// A primitive shared string implementation that does not
// move its .c_str() when the object is copied or moved.
struct shared_str {
	std::shared_ptr<string> content;
	shared_str() { }
	shared_str(string s) { content = std::shared_ptr<string>(new string(s)); }
	shared_str(const char *s) { content = std::shared_ptr<string>(new string(s)); }
	const char *c_str() const { return content->c_str(); }
	const string &str() const { return *content; }
	bool operator==(const shared_str &other) const { return *content == *other.content; }
	unsigned int hash() const { return hashlib::hash_ops<std::string>::hash(*content); }
};

using hashlib::mkhash;
using hashlib::mkhash_init;
using hashlib::mkhash_add;
using hashlib::mkhash_xorshift;
using hashlib::hash_ops;
using hashlib::hash_cstr_ops;
using hashlib::hash_ptr_ops;
using hashlib::hash_obj_ops;
using hashlib::dict;
using hashlib::idict;
using hashlib::pool;

namespace RTLIL {
	struct IdString;
	struct Const;
	struct SigBit;
	struct SigSpec;
	struct Wire;
	struct Cell;
	struct Module;
	struct Design;
	struct Monitor;
}

namespace AST {
	struct AstNode;
}

using RTLIL::IdString;
using RTLIL::Const;
using RTLIL::SigBit;
using RTLIL::SigSpec;
using RTLIL::Wire;
using RTLIL::Cell;
using RTLIL::Module;
using RTLIL::Design;

namespace hashlib {
	template<> struct hash_ops<RTLIL::Wire*> : hash_obj_ops {};
	template<> struct hash_ops<RTLIL::Cell*> : hash_obj_ops {};
	template<> struct hash_ops<RTLIL::Module*> : hash_obj_ops {};
	template<> struct hash_ops<RTLIL::Design*> : hash_obj_ops {};
	template<> struct hash_ops<RTLIL::Monitor*> : hash_obj_ops {};
	template<> struct hash_ops<AST::AstNode*> : hash_obj_ops {};

	template<> struct hash_ops<const RTLIL::Wire*> : hash_obj_ops {};
	template<> struct hash_ops<const RTLIL::Cell*> : hash_obj_ops {};
	template<> struct hash_ops<const RTLIL::Module*> : hash_obj_ops {};
	template<> struct hash_ops<const RTLIL::Design*> : hash_obj_ops {};
	template<> struct hash_ops<const RTLIL::Monitor*> : hash_obj_ops {};
	template<> struct hash_ops<const AST::AstNode*> : hash_obj_ops {};
}

void memhasher_on();
void memhasher_off();
void memhasher_do();

extern bool memhasher_active;
inline void memhasher() { if (memhasher_active) memhasher_do(); }

void yosys_banner();
std::string stringf(const char *fmt, ...) YS_ATTRIBUTE(format(printf, 1, 2));
std::string vstringf(const char *fmt, va_list ap);
int readsome(std::istream &f, char *s, int n);
std::string next_token(std::string &text, const char *sep = " \t\r\n", bool long_strings = false);
std::vector<std::string> split_tokens(const std::string &text, const char *sep = " \t\r\n");
bool patmatch(const char *pattern, const char *string);
int run_command(const std::string &command, std::function<void(const std::string&)> process_line = std::function<void(const std::string&)>());
std::string make_temp_file(std::string template_str = "/tmp/yosys_XXXXXX");
std::string make_temp_dir(std::string template_str = "/tmp/yosys_XXXXXX");
bool check_file_exists(std::string filename, bool is_exec = false);
bool is_absolute_path(std::string filename);
void remove_directory(std::string dirname);

template<typename T> int GetSize(const T &obj) { return obj.size(); }
int GetSize(RTLIL::Wire *wire);

extern int autoidx;
extern int yosys_xtrace;

YOSYS_NAMESPACE_END

#include <ext-libs/yosys/utils/log.hpp>
#include <ext-libs/yosys/utils/rtlil.hpp>
#include <ext-libs/yosys/utils/register.hpp>

YOSYS_NAMESPACE_BEGIN

using RTLIL::State;

namespace hashlib {
	template<> struct hash_ops<RTLIL::State> : hash_ops<int> {};
}


// CirKit Note: Do not use these functions. 
// Instead use start_yosys(), stop_yosys() and is_yosys_started() in yosys_utils.hpp
void yosys_setup();
void yosys_shutdown();

#ifdef YOSYS_ENABLE_TCL
Tcl_Interp *yosys_get_tcl_interp();
#endif

extern RTLIL::Design *yosys_design;

RTLIL::IdString new_id(std::string file, int line, std::string func);

#define NEW_ID \
	YOSYS_NAMESPACE_PREFIX new_id(__FILE__, __LINE__, __FUNCTION__)

#define ID(_str) \
	([]() { static YOSYS_NAMESPACE_PREFIX RTLIL::IdString _id(_str); return _id; })()

RTLIL::Design *yosys_get_design();
std::string proc_self_dirname();
std::string proc_share_dirname();
const char *create_prompt(RTLIL::Design *design, int recursion_counter);
void rewrite_filename(std::string &filename);

void run_pass(std::string command, RTLIL::Design *design = nullptr);
void run_frontend(std::string filename, std::string command, std::string *backend_command, std::string *from_to_label = nullptr, RTLIL::Design *design = nullptr);
void run_frontend(std::string filename, std::string command, RTLIL::Design *design = nullptr);
void run_backend(std::string filename, std::string command, RTLIL::Design *design = nullptr);
void shell(RTLIL::Design *design);

// from kernel/version_*.o (cc source generated from Makefile)
extern const char *yosys_version_str;

extern RTLIL::Design * abc_parse_blif (FILE *f, std::string dff_name);

// from passes/cmds/design.cc
extern std::map<std::string, RTLIL::Design*> saved_designs;
extern std::vector<RTLIL::Design*> pushed_designs;

// from passes/cmds/pluginc.cc
extern std::map<std::string, void*> loaded_plugins;
extern std::map<std::string, std::string> loaded_plugin_aliases;
void load_plugin(std::string filename, std::vector<std::string> aliases);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Cirkit Utils
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
inline std::string proper_name (const std::string wire_name) {
  return boost::erase_all_copy (wire_name, "\\");
}

//------------------------------------------------------------------------------
// A wrapper with try-catch. TODO: enhance/correct this later.
inline void run_yosys_cmd (const std::string &command, bool print_cmd = true) {
  if (print_cmd) std::cout << "[i] Yosys :: " << command << std::endl;
  try {  Yosys::run_pass(command); }
  catch (const std::exception& e) {std::cout << e.what() << "\n";}
  catch (...) { throw; }
}

// Note: Instead of this function, use is_yosys_started() in yosys_utils.hpp
//---// TODO implement this one...
//---inline bool is_yosys_initialized() {
//---  // should be true even after design -reset.
//---  // only indiicate if its initialized or not, not if it has a valid design or not.
//---  //return __cirkit_yosys_status;
//---  return true;
//---}

inline bool has_module ( const std::string & module_name, RTLIL::Design *design) {
  RTLIL::Module* module;
  auto flag = false;
  for ( auto mod : design->modules() ) {
    if ( proper_name ( (mod->name).str() ) == module_name) {
      module = mod; flag = true;
      break;
    }
  }
  if (!flag) std::cout << "[e] Cannot find the module " << module_name
		       << " in the design" << std::endl;
  return flag;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
