// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_to_ntk.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "aig_to_ntk.hpp"

//namespace abc {
//Aig_Man_t *         Gia_ManToAigSimple( Gia_Man_t * p );
//}

namespace alice
{

aig_to_ntk_command::rules_t aig_to_ntk_command::validity_rules() const {
  return {
    { [this]() {
	const auto& aigs = env->store<abc::Gia_Man_t*>();
	return id < aigs.size();
      }
      , "Id out of range!"  },

      //rule2
    { [this]() {
	const auto& aigs = env->store<abc::Gia_Man_t*>();
	return !aigs.empty();
      }
      , "No AIG in the store!"  }

      
      };
}

bool aig_to_ntk_command::execute() {
  auto& gias = env->store <abc::Gia_Man_t *>();
  auto gia = gias[id];
  if (gia == nullptr) return false;
  auto is_seq_ckt = false;
  if ( gia->nRegs > 0 ) is_seq_ckt = true;
  auto ntk = axekit::convert_gia_to_ntk (gia);
  /*
    // below is the actual procedure. But its a segfault.
    // hence taking this route.
  auto aigman = abc::Gia_ManToAig (gia, 0);
  auto ntk =  abc::Abc_NtkFromAigPhase ( aigman );
  */

  // if there are consecutive conversions, below one may pick wrong file!
  //--auto orig_file = std::string ( abc::Gia_ManName ( gia ) + std::string (".aig") );
  //--boost::filesystem::path orig_path (orig_file);
  //--std::string ntk_file = "/tmp/tmp_readaig.aig";
  //--if ( boost::filesystem::exists (orig_path) ) {
  //--  ntk_file = orig_file;
  //--}
  //--else {
  //--  abc::Gia_AigerWrite ( gia, (char*)ntk_file.c_str(), 1, 0 );    
  //--}

  //--std::string ntk_file = "/tmp/tmp_readaig.aig";
  //--abc::Gia_AigerWrite ( gia, (char*)ntk_file.c_str(), 1, 0 );    
  //--auto ntk = abc::Io_ReadAiger ( (char *)ntk_file.c_str(), 1 );
  //--ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  //--ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  //--abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  //--ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  //--ntk->pName = abc::Abc_UtilStrsav ( gia->pName );
  if ( is_seq_ckt ) { assert ( abc::Abc_NtkLatchNum (ntk) != 0 );  } 

  auto& ntks = env->store <abc::Abc_Ntk_t *>();
  if ( is_set("new") || ntks.empty() ) {
    ntks.extend();
  }
  ntks.current() = ntk;
  return true;
}

aig_to_ntk_command::log_opt_t aig_to_ntk_command::log() const  {
  return log_opt_t (
    {
      {"aig_id", id}
    }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
