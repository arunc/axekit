// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : verilog_io.cpp
// @brief  : implementation of the read_verilog and write_verilog commands.
//           Uses yosys.  Read yosys_utils.hpp before doing more functions with yosys.
//           
//------------------------------------------------------------------------------

// TODO: verilog write procedure from gia
//   auto kk = abc::Gia_ManToAig (xg, 0);
//   abc::Aig_ManDumpVerilog (kk, tmpv);


#include "verilog_io.hpp"
#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>
#include <fstream>
#include <iostream>

namespace alice
{

namespace {
// Private functions.
// predicate for validity-rule 1.
bool check_extension (const std::string &file) {
  auto ext = axekit::get_extension_of_file (file);
  if (ext == ".v") return true;
  if (ext == ".V") return true;

  return false;
}

// predicate for validity-rule 2.
// Grep and see if there is a "module top_module" in the file.
// This will save some debugging effort later.
bool check_top_module (const std::string &file, const std::string &top_module) {
  static const boost::regex top_check (".*module\\s+" + top_module + "[( ].*");
  std::ifstream infile (file);
  std::string line;
  while ( std::getline (infile, line) ) {
    if ( boost::regex_match (line, top_check) ) return true;
  }
  return false;
}
}

read_verilog_command::rules_t read_verilog_command::validity_rules() const {
  return {
    // rules are pairs, first is a predicate and, next is a string when predicate is false.
    // RULE FORMAT::    { [this]() {} , ""  }
    { [this]() {
	boost::filesystem::path p(file);
	return boost::filesystem::exists (p);
      }
      , "Where is the input file?"  },

    // rule-1. file should have verilog extension.
    { [this]() { return check_extension (file); }
      , "Not a VERILOG? Extension should be .v or .V!"  },
    { [this]() { return (top_module != "123_") ; }
      , "Top module not provided!"  },
    { [this]() { return check_top_module (file, top_module); }
      , "The top-module specified " + top_module + " does NOT exist!"  },
    { [this]() { return (store == 2 || store == 0) ; }
      , "Store must be 0 (Gia) or 2 (Old Aig Network). BDD not supported."  }
  };
}

bool read_verilog_command::execute() {
  // step 1. parse verilog, optimize, flatten, and write a tmp blif
  std::string tmp_blif = "/tmp/tmp.blif";
  auto begin_time = std::clock ();
  
  //--------------------------------------------------------------------------------------
  // YOSYS BLOCK - do not offload to a separate function
  //--------------------------------------------------------------------------------------
  std::ofstream out(debug_log);
  if (debug_log != "NA") {
    //Yosys::log_streams.push_back(&std::cout);
    //Yosys::log_error_stderr = true;
    Yosys::log_streams.push_back(&out);
    Yosys::log_error_stderr = true;
  }
  
  Yosys::yosys_setup();  // repeated calling of yosys_setup() is not a problem.
  auto design = Yosys::yosys_get_design();
  Yosys::Pass::call (design, Yosys::stringf("design -reset") );
  Yosys::Frontend::frontend_call (design, nullptr, file, "verilog");
  Yosys::Pass::call (design, Yosys::stringf("hierarchy -check -top %s", top_module.c_str()));

  // coarse synthesis
  Yosys::Pass::call(design, "proc");
  Yosys::Pass::call(design, "opt");
  Yosys::Pass::call(design, "wreduce");
  Yosys::Pass::call(design, "alumacc");
  Yosys::Pass::call(design, "share");
  Yosys::Pass::call(design, "opt");
  Yosys::Pass::call(design, "fsm");
  Yosys::Pass::call(design, "opt -fast");
  Yosys::Pass::call(design, "memory -nomap");
  Yosys::Pass::call(design, "opt_clean");

  // flatten design 
  Yosys::Pass::call(design, "flatten");
  Yosys::Pass::call(design, "opt_clean");

  // fine synthesis
  Yosys::Pass::call(design, "opt -fast -full");
  Yosys::Pass::call(design, "memory_map");
  Yosys::Pass::call(design, "opt -full");
  Yosys::Pass::call(design, "techmap");
  Yosys::Pass::call(design, "opt -fast");
  Yosys::Pass::call(design, "opt_clean");

  Yosys::Backend::backend_call (design, nullptr, tmp_blif, "blif");
  Yosys::Pass::call (design, Yosys::stringf("design -reset") );
  //Yosys::yosys_shutdown(); // Note: never shutdown yosys. cannot initliaze later.
  //--------------------------------------------------------------------------------------
  //--------------------------------------------------------------------------------------
  
  // step 2. read tmp blif the traditional way and dump an aiger
  auto ntk = abc::Io_ReadBlif ( (char *)tmp_blif.c_str(), 1 );
  std::string tmp_aig = "/tmp/tmp.aig";
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  if (ntk == nullptr) std::cout << "Null pointer!\n";
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again

  auto is_seq_ckt = false;
  if ( abc::Abc_NtkLatchNum (ntk) != 0 ) {
    std::cout << "[i] Circuit: " << top_module << " is sequential. \n";
    is_seq_ckt = true;
  }
  
  if (2 == store) {
    auto& aigs = env->store <abc::Abc_Ntk_t *>();
    if ( is_set("new") || aigs.empty() ) {
      aigs.extend();
    }
    ntk->pName = abc::Abc_UtilStrsav ( (char *) top_module.c_str() );
    aigs.current() = ntk;
    t_read = get_elapsed_time (begin_time);
    return true;

  }
  
  if (0 == store) {
    abc::Io_WriteAiger (ntk, (char *)tmp_aig.c_str(), 1, 0, 0);
    
    // step 3. read the aig and put it to the store.
    auto& aigs = env->store <abc::Gia_Man_t *>();
    if ( is_set("new") || aigs.empty() ) {
      aigs.extend();
    }
    // do the below in 2 steps. Else errors will not be easy to understand.
    auto gia = abc::Gia_AigerRead( (char *)tmp_aig.c_str(), 1, 1, 1 );
    gia->pName = abc::Abc_UtilStrsav ( (char *) top_module.c_str() );
    if ( is_seq_ckt ) { assert ( gia->nRegs > 0 ); } // Just make sure read in ckt is also sequential.
    aigs.current() = gia;
    t_read = get_elapsed_time (begin_time);
    return true;
  }

  return false; // shud not reach this stage.
  
}

read_verilog_command::log_opt_t read_verilog_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      {"top_module", top_module},
      {"store_used", (store == 0u) ? std::string("GIA") : std::string("NTK") },
      {"time_taken", boost::lexical_cast<std::string> (t_read) }
    }
    );
}


} // namespace alice

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
