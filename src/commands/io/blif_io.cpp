// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : blif_io.cpp
// @brief  : implementation of the read_blif and write_blif commands.
//------------------------------------------------------------------------------

#include "blif_io.hpp"
#include <ext-libs/abc/abc_api.hpp>


namespace alice
{

namespace {
// private functions
// predicate for validity rule 1.
bool check_extension (const std::string &file) {
  auto ext = axekit::get_extension_of_file (file);
  if (ext == ".blif") return true;
  if (ext == ".BLIF") return true;

  return false;
}
}

read_blif_command::rules_t read_blif_command::validity_rules() const {
  return {
    // rules are pairs, first is a predicate and, next is a string when predicate is false.
    // RULE FORMAT::    { [this]() {} , ""  }
    // rule-1. file should have blif extension.

    { [this]() {
	boost::filesystem::path p(file);
	return boost::filesystem::exists (p);
      }
      , "Where is the input file?"  },


    { [this]() { return check_extension (file); } , "Not a BLIF? Extension should be .blif or .BLIF!"  },
    { [this]() { return (store == 2 || store == 0 || is_set ("nanotrav") ) ; }
      , "Store must be 0 (Gia) or 2 (Old Aig Network) or nantrav option must be set"  }
    
  };
}


bool read_blif_command::execute() {

  if ( is_set ("nanotrav") ) { // Directly read the blif using nanotrav
    auto begin_time = std::clock ();
    auto& bdds = env->store <cirkit::bdd_function_t>();
    
    Cudd mgr;
    if ( !bdds.empty() ) {
      std::cout << "[w] All inputs common to all BDDs. (Unpredictable behavior otherwise)\n";
    }
    if ( !bdds.empty() ) mgr = bdds[0].first;
    if ( is_set("new") || bdds.empty() ) {
      bdds.extend();
    } 
    
    auto bddf = axekit::read_nanotrav_blif (file, mgr);
    bdds.current() = bddf;
    t_read = get_elapsed_time (begin_time);
    return true;
  }

  char name[] = "temp";
  auto begin_time = std::clock ();
  // read blif the traditional way and dump an aiger
  auto ntk = abc::Io_ReadBlif ( (char *)file.c_str(), 1 );
  std::string tmp_file = "/tmp/tmp.aig";
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  if (ntk == nullptr) std::cout << "Null pointer!\n";
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  ntk->pName = abc::Abc_UtilStrsav ( name );
    
  auto is_seq_ckt = false;
  if ( abc::Abc_NtkLatchNum (ntk) != 0 ) {
    std::cout << "[i] Read in circuit is sequential. \n";
    is_seq_ckt = true;
  }

  if (0 == store) {
    abc::Io_WriteAiger (ntk, (char *)tmp_file.c_str(), 1, 0, 0);
    // read the aig and put it to the store.
    auto& gias = env->store <abc::Gia_Man_t *>();
    if ( is_set("new") || gias.empty() ) {
      gias.extend();
    }
    // do the below in 2 steps. Else errors will not be easy to understand.
    auto gia = abc::Gia_AigerRead( (char *)tmp_file.c_str(), 1, 1, 1 );
    gia->pName = abc::Abc_UtilStrsav ( name ); 
    if ( is_seq_ckt ) { assert ( gia->nRegs > 0 ); } // Just make sure read in ckt is also sequential.
    gias.current() = gia;
    t_read = get_elapsed_time (begin_time);
    return true;
  }

  if (2 == store) {
    auto& ntks = env->store <abc::Abc_Ntk_t *>();
    if ( is_set("new") || ntks.empty() ) {
      ntks.extend();
    }
    ntks.current() = ntk;
    t_read = get_elapsed_time (begin_time);
    return true;
  }
  
  std::cout << "[w] Unimplemented feature. Use --nanotrav or conversion commands\n";
  return false;
}


read_blif_command::log_opt_t read_blif_command::log() const  {
  return log_opt_t (

    {
      {"file", file},
      {"time_taken", boost::lexical_cast<std::string> (t_read) }
    }
    
    );
}





write_blif_command::rules_t write_blif_command::validity_rules() const {
  
  return {
    { [this]() { return (store >= 0 && store <3) ; }, "Invalid store entry."  }
  };
}


bool write_blif_command::execute() {
  auto begin_time = std::clock ();
  if (store == 0) { // Gia
    auto& gias = env->store <abc::Gia_Man_t *>();
    if ( gias.empty() ) {
      std::cout << "[e] Empty Network store\n";
      return false;
    }
    if ( id >= gias.size() ) {
      std::cout << "[e] Id out of range\n";
      return false;
    }
    auto gia = gias[id];
    if (gia == nullptr) return false;

    // convert to old network first.
    auto ntk =  abc::Abc_NtkFromAigPhase ( abc::Gia_ManToAig (gia, 0) );
    if (ntk == nullptr) return false;
    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
    abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
    ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
    if (ntk == nullptr) return false;

    abc::Io_WriteBlif ( abc::Abc_NtkToNetlist (ntk),
			(char *)file.c_str(), 0, 0, 0 );
    t_write = get_elapsed_time (begin_time);
    return true;
  }
  if (store == 1) { // BDD
    auto& bdds = env->store <cirkit::bdd_function_t>();
    if ( bdds.empty() ) {
      std::cout << "[e] Empty BDD store\n";
      return false;
    }
    if ( id >= bdds.size() ) {
      std::cout << "[e] Id out of range\n";
      return false;
    }
    auto bddf = bdds[id];
    auto res = write_nanotrav_blif (file, bddf, "top");
    if (!res) std::cout << "[e] Error in writing BLIF!\n";
    t_write = get_elapsed_time (begin_time);
    return res;
  }
  if (store == 2) { // Network 
    auto& ntks = env->store <abc::Abc_Ntk_t*>();
    if ( ntks.empty() ) {
      std::cout << "[e] Empty Network store\n";
      return false;
    }
    if ( id >= ntks.size() ) {
      std::cout << "[e] Id out of range\n";
      return false;
    }
    auto ntk = ntks[id];
    abc::Io_WriteBlif ( abc::Abc_NtkToNetlist (ntk),
			(char *)file.c_str(), 0, 0, 0 );
    t_write = get_elapsed_time (begin_time);
    return true;
  }

  return false; // shud not reach here.
}


write_blif_command::log_opt_t write_blif_command::log() const  {
  return log_opt_t (

    {
      {"file", file},
      {"id", boost::lexical_cast<std::string> (id) },
      {"store_used", (store == 0) ? std::string("AIG") :
	  ( (store == 1)  ? std::string ("BDD") : std::string("Network")   )   },
      {"time_taken", boost::lexical_cast<std::string> (t_write) }
    }
    
    );
}






} // namespace alice

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
