// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : blif_io.hpp
// @brief  : read blif (TODO: write_blif also)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef BLIF_IO_HPP
#define BLIF_IO_HPP

// ABC needs LIN64 macro
#define LIN64

#include <base/abc/abc.h>
#include <base/io/ioAbc.h>

#include <boost/format.hpp>
#include <commands/axekit_stores.hpp>

#include <utils/common_utils.hpp>
#include <utils/nanotrav/blif_nanotrav_io.hpp>
#include <boost/filesystem.hpp>

namespace alice {

class read_blif_command : public command {
public:
  read_blif_command (const environment::ptr &env) : command (env, "Read BLIF format and store in Aig")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f", po::value (&file),              "The input BLIF file" )
      ( "store,s",      po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / Ntk=2 (Old style))" )
      ( "nanotrav",    "Directly read the blif to a BDD store using nanotrav. Ignores --store" )
      ( "new,n" ,                                   "Create a new store entry" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  int store = 0;
  std::string file;
  float t_read = -1;
};



class write_blif_command : public command {
public:
  write_blif_command (const environment::ptr &env) : command (env, "Write the circuit in BLIF format")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f", po::value (&file),              "The output BLIF file" )
      ( "id",         po::value( &id )->default_value( id ), "Store id of the network" )
      ( "store,s",    po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / BDD=1 / Ntk=2)" )
      ; 
  }

protected:
  rules_t validity_rules() const;
  // the main execution part of the command.
  bool execute(); 
  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  int id = 0;
  std::string file;
  int store = 1;
  float t_write = -1;
};




}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
