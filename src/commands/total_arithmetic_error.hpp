// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : total_arithmetic_error.hpp
// @brief  : Sum of all errors (this divided by number of input combinations is avg-case)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef TOTAL_ARITHMETIC_ERROR_HPP
#define TOTAL_ARITHMETIC_ERROR_HPP

#include <commands/axekit_stores.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/gia/gia_countsat.hpp>
#include <functions/gia/gia_lexsat.hpp>

namespace alice {

class total_arithmetic_error_command : public command {
public:
  total_arithmetic_error_command (const environment::ptr &env) : command (env, "Reports the total arithmetic error")
  {
    opts.add_options()
      ( "id", po::value( &id )->default_value( id ), "store id of diff circuit" )
      ( "store,s", po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / BDD=0)" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  unsigned id = 0;
  unsigned store = 0;

  // results variables.
  boost::multiprecision::uint256_t total_error = 0;
  float sum_time = -1;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
