
//-----------------------ORIGINAL DESIGN BEGIN- ----------------------------
module RCA_N16(
    input [15:0] in1,
    input [15:0] in2,
    output [16:0] res
    );

assign res[16:0] = in1[15:0] + in2[15:0];

endmodule

//-----------------------ORIGINAL DESIGN END- ----------------------------
/*
 @file       :  seq_rca_n16.v
 @author     :  Arun <arun@uni-bremen.de>
 @brief      :  Seq Multiplier with Approximate Adder
 
 Modifications :
   Instead of the "+" operator explicit adder is used to faciliate the
   study of approximate adders.
 
 */
   module adder (
                output [16:0] res, 
                input [15:0] in1, 
                input [15:0] in2
                );

   RCA_N16 approx_adder_inst (.res(res), 
       		             .in1(in1), .in2(in2) );

   endmodule





module multiplier (a, b, out, clk, rst, oe);
   output [15:0] out;
   output 	 oe;
   input [7:0] 	 a;
   input [7:0] 	 b;
   input 	 clk;
   input 	 rst;

   reg [15:0] 	 out;
   reg 		 oe;
   wire [7:0] 	 a;
   wire [7:0] 	 b;
   wire 	 clk;
   wire 	 rst;


   wire [15:0] 	 p0, p1, p2, p3, p4, p5, p6, p7; // 8 Partial Products

   reg [7:0] 	 a_reg;
   reg [7:0] 	 b_reg;
   
   
   assign p0 = b_reg[0] ? {8'b0, a_reg} : 16'b0;
   assign p1 = b_reg[1] ? {7'b0, a_reg, 1'b0} : 16'b0;
   assign p2 = b_reg[2] ? {6'b0, a_reg, 2'b0} : 16'b0;
   assign p3 = b_reg[3] ? {5'b0, a_reg, 3'b0} : 16'b0;
   assign p4 = b_reg[4] ? {4'b0, a_reg, 4'b0} : 16'b0;
   assign p5 = b_reg[5] ? {3'b0, a_reg, 5'b0} : 16'b0;
   assign p6 = b_reg[6] ? {2'b0, a_reg, 6'b0} : 16'b0;
   assign p7 = b_reg[7] ? {1'b0, a_reg, 7'b0} : 16'b0;
   

   // Now each cycle the partial products are simply added.
   reg [15:0] 	 in1;
   reg [15:0] 	 in2;
   wire [16:0] 	 res;
   
   adder adder_inst1 (.in1(in1), .in2(in2), .res(res));

   reg [5:0] 	 state;
   
   always @(posedge clk)
     begin
	if (rst)
	  begin
	     state <= 6'd0; oe <= 0; out <= 16'd0; in1 <= 16'd0; in2 <= 16'd0;
	     a_reg <= 8'b0; b_reg <= 8'b0;
	  end
	else
	  begin
	     case (state)
	       0:
		 begin
		    // Mix these non-linear conditions for loop unrolling differences.
		    if (b == 8'd0)
		      begin
			 state <= 6'd0; out <= 16'd0; oe <= 1; 
		      end
		    else if (b == 8'b1)
		      begin
			 state <= 6'd0; out <= {8'd0, a}; oe <= 1; 
		      end
		    else if (a == 8'b0)
		      begin
			 state <= 6'd0; out <= 16'd0; oe <= 1; 
		      end
		    else if (a == 8'b1)
		      begin
			 state <= 6'd0; out <= {8'd0, b}; oe <= 1; 
		      end
		    else
		      begin
		  
			 a_reg <= a;
			 b_reg <= b;
			 oe <= 0; out <= 16'd0;
			 state <= 6'd1;
		      end // else: !if(b == 8'b1)
		 end // case: 0
	       1:
		 begin
		    in1 <= p0; in2 <= p1; state <= 2; oe <= 0; out <= 16'd0;
		 end
	       2:
		 begin
		    in1 <= res[15:0]; in2 <= p2; state <= 3; oe <= 0; out <= 16'd0;
		 end
	       3:
		 begin
		    in1 <= res[15:0]; in2 <= p3; state <= 4; oe <= 0; out <= 16'd0;
		 end
	       4:
		 begin
		    in1 <= res[15:0]; in2 <= p4; state <= 5; oe <= 0; out <= 16'd0;
		 end
	       5:
		 begin
		    in1 <= res[15:0]; in2 <= p5; state <= 6; oe <= 0; out <= 16'd0;
		 end
	       6:
		 begin
		    in1 <= res[15:0]; in2 <= p6; state <= 7; oe <= 0; out <= 16'd0;
		 end
	       7:
		 begin
		    in1 <= res[15:0]; in2 <= p7; state <= 8; oe <= 0; out <= 16'd0;
		 end
	       8:
		 begin
		    out <= res[15:0];
		    state <= 0; oe <= 1;
		 end
	       default:
		 begin
		    state <= 0; oe <= 0;
		 end  
	     endcase // case (state)
	  end // else: !if(rst)
     end // always @ (posedge clk)
      
   
   


endmodule // multiplier



